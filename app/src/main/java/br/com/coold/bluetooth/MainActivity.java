package br.com.coold.bluetooth;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.util.Set;
import java.util.UUID;


public class MainActivity extends AppCompatActivity {

    static EditText texto;
    static Button botaoEnviar;
    public static final int ENABLE_BLUETOOTH = 1;
    public static final int SELECT_PAIRED_DEVICE = 2;
    public static final int SELECT_DISCOVERED_DEVICE = 3;
    public static final int MY_PERMISSIONS_REQUEST_BLUETOOTH = 1717;
    private BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
    private ListView lista;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        botaoEnviar = (Button) findViewById(R.id.BtnEnviar);
        botaoEnviar.setEnabled(false);
        lista = (ListView) findViewById(R.id.lstPareados);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_BLUETOOTH: {
                if (grantResults.length > 0) {
                    if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                        ativaBluetooth(btAdapter);
                        botaoEnviar.setEnabled(true);
                        popularListaPareados(btAdapter);
                    } else {
                        if (grantResults[0] == PackageManager.PERMISSION_DENIED) {
                            mostrarTexto("Permissão negada");
                        }
                    }
                }
                return;
            }
        }
    }

    public void mostrarTexto(String texto){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(texto);
        builder.setTitle(texto);
        AlertDialog alerta = builder.create();
        alerta.show();
    }


    private boolean isBluetoothPermissionsGranted(){
        int bluetoothGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH);
        int bluetoothAdminGranted = ContextCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH_ADMIN);

        if (bluetoothGranted == PackageManager.PERMISSION_GRANTED && bluetoothAdminGranted == PackageManager.PERMISSION_GRANTED){
            return true;
        }else{
            return false;
        }

    }


    private void askForBluetoothPermissions(){
        String [] permissions = new String[]{Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN};
        ActivityCompat.requestPermissions(this, permissions, MY_PERMISSIONS_REQUEST_BLUETOOTH);
    }

    private void ativaBluetooth(BluetoothAdapter btAdapter){
        try {
            if (!btAdapter.isEnabled()) {
                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                startActivityForResult(enableBtIntent, ENABLE_BLUETOOTH);
            }
        }catch (Exception e){
            mostrarTexto(e.getMessage());
        }
    }

    private void popularListaPareados(final BluetoothAdapter btAdapter){
        Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();

// If there are paired devices

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_selectable_list_item);

        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                adapter.add(device.getName() + "\n" + device.getAddress());
            }
        }
        lista.setAdapter(adapter);
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String item = (String) lista.getAdapter().getItem(position);
                String devName = item.substring(0, item.indexOf("\n"));
                String devAddress = item.substring(item.indexOf("\n")+1, item.length());
//                mostrarTexto(devName + " "+devAddress);
//
                try {
                    BluetoothDevice btDevice = btAdapter.getRemoteDevice(devAddress);
                    BluetoothSocket btsocket = btDevice.createRfcommSocketToServiceRecord(btDevice.getUuids()[0].getUuid());
//                    mostrarTexto(btsocket.getRemoteDevice().getName());
//                    btsocket.connect();
                    String mensagem = "TESTE";
                    btsocket.getOutputStream().write(mensagem.getBytes());
                } catch (Exception e) {
                    mostrarTexto(e.getMessage());
                }
            }
        });
    }

    public void btAtivarClick(View view){

        if (btAdapter instanceof BluetoothAdapter){
            if (!isBluetoothPermissionsGranted()) {

                askForBluetoothPermissions();
            }
        }
    }

    public void btEnviarClick(View view){

    }

}
